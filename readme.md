Features

* include in class with one string
* class name logging
* transaction info logging
* multi loggers support (use several log objects in single class)

Roadmap

* Rake tasks for build,install, check install and require gem, run examples. gem:build, gem:install, run:examples, gem:check_install
* Test and rake task for tests
* Add license file. http://choosealicense.com/licenses/mit/
* Support GELF format
