require 'rake'

Gem::Specification.new do |s|
  s.name        = 'ext_logging'
  s.version     = '0.0.0'
  s.date        = '2014-05-26'
  s.summary     = 'Extended features for logging'
  s.description = 'Event tracing, class tracing in your logs'
  s.authors     = ['Alexander Popov']
  s.email       = 'alexgpg@gmail.com'
  s.files       = FileList['lib/*.rb', 'examples/*.rb', '*.md']
  s.homepage    =
    'https://bitbucket.org/alexgpg/ext_logging'
  s.license       = 'MIT'
end
