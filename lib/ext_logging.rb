require_relative 'log_wrapper'
require_relative 'event'

module ExtLogging
  def self.included(mod)
    puts "included #{mod}"
    mod.extend(self)

    #mod.define_singleton_method
  end

  # End point for use ext logging
  def ext_logger
    talker = self
    logger_wrapper = LoggerWrapper.new(talker)

    if @logger
      logger_wrapper.logger = @logger
    else
      logger_wrapper.logger = default_output_logger()
    end
    logger_wrapper
  end

  def default_output_logger
    nil
  end

  def ext_logger_default_logger &block
    define_method 'default_output_logger' do
      block.call
    end

    define_singleton_method 'default_output_logger' do
      block.call
    end

    #puts "ext_logger_default_logger: SELF: #{self}: #{self.class}"
  end


  # todo: attr_accessor ?
  # Inject logger in class or object
  def logger= logger
    #raise 'not implemented'
    @logger = logger
  end

  def logger
    @logger
  end
end


