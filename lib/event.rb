require 'digest'
require 'securerandom'

module ExtLogging
  module Event
    def event_uid
      @uuid ||= Digest::SHA1.hexdigest(SecureRandom.random_bytes(10))[0..7]
    end

    def parent_event_uid

    end

    def event_type

    end

    # TODO: IP or login or email
    #def user
    #
    #end
  end
end
