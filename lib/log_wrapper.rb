require 'json'

module ExtLogging
  class LoggerWrapper

    attr_accessor :logger

    def initialize(talker)
      @talker = talker
    end

    def [] logger
      self.logger = logger
      self
    end

    # DEBUG < INFO < WARN < ERROR < FATAL < UNKNOWN
    [:debug, :info, :warn, :error, :fatal, :unknown].each do |method_name|
      define_method(method_name) do |*args|
        log(method_name, *args)
      end
    end

    def log(severity, *args)
      raise 'Not output logger set' if @logger.nil?
      if args[0].is_a?(Event)
        op = args[0]
        msg = args[1]
        obj = args[2]

        op_str_part = ""

        parent_op_id = op.parent_event_uid

        if parent_op_id
          op_str_part += "[#{op.event_uid.to_s}]<#{parent_op_id}]"
        else
          op_str_part += "[#{op.event_uid.to_s}]"
        end
        op_str_part += " (#{op.event_type})" if op.event_type
      elsif args[0].is_a?(String)
        msg = args[0]
        obj = args[1]

        op_str_part = ''
      else
        raise 'bad arg count'
      end

      obj_part = "=> #{JSON.dump(obj.to_hash)}" if obj

      if @talker.is_a?(Class) || @talker.is_a?(Module)
        class_part = @talker.to_s
      else
        class_part = @talker.class
      end

      @logger.info "#{op_str_part} <#{class_part}> | #{msg} |#{obj_part}"
    end
  end
end

