#!/bin/env ruby

require 'logger'
require_relative '../lib/ext_logging'

# Set deafault logger for class
class Example
  include ExtLogging

  # Run every log. This bad place for creatting logger. Return already created
  ext_logger_default_logger { $default_logger }

  def op_1
    ext_logger.info 'op1 example message'
  end

  def self.class_op
    ext_logger.info 'class_op'
  end
end

# Create loggers
$default_logger = Logger.new(STDOUT)
$default_logger.formatter = proc { |severity, datetime, progname, msg|
  original_formatter = Logger::Formatter.new
  "[DEFAULT] " + original_formatter.call(severity, datetime, progname, msg.to_s)
}

$logger4class = Logger.new(STDOUT)
$logger4class.formatter = proc { |severity, datetime, progname, msg|
  original_formatter = Logger::Formatter.new
  "[LOG4CLS] " + original_formatter.call(severity, datetime, progname, msg.to_s)
}

$logger4obj = Logger.new(STDOUT)
$logger4obj.formatter = proc { |severity, datetime, progname, msg|
  original_formatter = Logger::Formatter.new
  "[LOG4OBJ] " + original_formatter.call(severity, datetime, progname, msg.to_s)
}


# Use default logger
Example.class_op
o = Example.new
o.op_1

# Inject in class
Example.logger = $logger4class
Example.class_op
o = Example.new
o.op_1

# Inject in object
example_obj = Example.new
example_obj.logger = $logger4obj
Example.class_op
example_obj.op_1
