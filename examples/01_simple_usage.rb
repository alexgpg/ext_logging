#!/bin/env ruby

require 'logger'

require_relative '../lib/ext_logging'

class Rails
  @@logger

  def self.logger
    @@logger
  end

  def self.logger=(logger)
    @@logger = logger
  end
end

class Operation
  include ExtLogging::Event

  def initialize(type)
    @type = type
  end

  def event_type
    @type
  end

  def ip
    '81.91.180.150'
  end
end


module Video
  class ResourcesService
    include ExtLogging

    #ext_logger_default_logger { Rails.logger }
    #ext_logger_no_

    def initialize(org)
      @organization = org
    end

    def add_mvr
      op = Operation.new(:add)

      #ext_logger.info op, "Resources successfully allocated for org", org_name: @organization.name

      ext_logger[Rails.logger].info op, "Test custom logger"
    end

    def self.generate_resource
      op = Operation.new(:gen_res)
      ext_logger.info op, "Resource generated"
    end
  end
end

o = Object.new
def o.name
  'Transaero'
end


s = Video::ResourcesService.new(o)
$log = Logger.new(STDOUT)

Rails.logger = $log

# Inject logger
Video::ResourcesService.logger = $log

10.times do

  s.add_mvr
  s.class.generate_resource
  #s.meth
  #s.meth2
  #s.meth3

  #$log.debug "Something"
end
