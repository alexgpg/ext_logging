#!/bin/env ruby

require 'logger'

require_relative '../lib/ext_logging'

class UserOperation
  include ExtLogging::Event
end


class SomeProcessor
  include ExtLogging
  ext_logger_default_logger { $logger }
  
  def run
    op = UserOperation.new
    ext_logger.info op, 'start method run', username: 'example', company: 'umbrella'
    
    # Without trace uid
    ext_logger.info "another message"
    
    ext_logger.debug op, 'debug message'

    ext_logger.error op, 'some error'

    ext_logger.info op, 'end'
  end
end


$logger = Logger.new(STDOUT)

p = SomeProcessor.new

10.times do
  p.run
end
