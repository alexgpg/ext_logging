#!/bin/env ruby

require_relative '../lib/ext_logging'
require 'logger'


# Use several loggers in one class

# Fake Rails logger
class Rails
  @@logger

  def self.logger
    @@logger
  end

  def self.logger=(logger)
    @@logger = logger
  end
end

$log = Logger.new(STDOUT)
Rails.logger = $log

MONEY_LOGGER = Logger.new(STDOUT) 


class Operation
  include ExtLogging::Event

  def initialize(type)
    @type = type
  end

  def event_type
    @type
  end
end

module Money
  class ResourcesService
    include ExtLogging

    def add
      op = Operation.new(:add_money)

      #ext_logger.info op, "Resources successfully allocated for org", org_name: @organization.name

      ext_logger[Rails.logger].info op, "Test custom logger"

      ext_logger[MONEY_LOGGER].info op, "Added 10$"
    end

  end
end



s = Money::ResourcesService.new

10.times do
 s.add
end

